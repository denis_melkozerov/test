romanNumbersUdinitsy = ['', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX'];
romanNumbersDozens = ['', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC'];
romanNumbersHundreds = ['', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM'];

input_number.oninput = function () {
    setGreek();
    console.log('f');
};

function setGreek() {
    output_result.innerHTML = toGreek(input_number.value);
}

function toGreek(value) {
    let numberValue = parseInt(value);
    let t = 0;
    let counter = 0;
    let result = '';

    while(numberValue != 0) {
        counter++;
        number = numberValue % 10;
        numberValue = Math.floor(numberValue/10);
        result += convertNumber(number, counter);
    }
    return result = reverseStr(result);
}

function convertNumber(number, counter) {
    if(counter == 1)
        return convertUdinitsyNumber(number);
    if(counter == 2)
        return convertDozensNumber(number);
    if(counter == 3)
        return convertHundredsNumber(number);
    else
        return convertThousandsNumber(number);
}

function convertUdinitsyNumber(number) {
    return romanNumbersUdinitsy[number];
}

function convertDozensNumber(number) {
    return romanNumbersDozens[number];
}

function convertHundredsNumber(number) {
    return romanNumbersHundreds[number];
}

function convertThousandsNumber(number) {
    let thousandResult = '';
    while(number != 0) {
        thousandResult += 'M';
        number--;
    }
    return thousandResult;
}

function reverseStr(str) {
    return str.split("").reverse().join("");
}

function validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}